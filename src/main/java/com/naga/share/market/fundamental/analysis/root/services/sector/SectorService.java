package com.naga.share.market.fundamental.analysis.root.services.sector;

import java.util.List;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface SectorService {

	/**
	 * 
	 * Service method to get all available sectors
	 * 
	 * @return List<SectorResponse>
	 */
	public List<SectorResponseDTO> allSectors();
}
