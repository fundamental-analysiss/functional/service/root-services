package com.naga.share.market.fundamental.analysis.root.services.company;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	CompanyDAO companyDAO;

	@Override
	public List<CompanyResponseDTO> allCompanies() {
		logger.info("CompanyServiceImpl - allCompanies method - start");
		List<CompanyResponseDTO> allCompanies = new ArrayList<CompanyResponseDTO>();
		for (Company company : companyDAO.findAllCompanies()) {
			CompanyResponseDTO companyRes = new CompanyResponseDTO();
			companyRes.setCompanyName(company.getCompanyName());
			companyRes.setCompanySymbol(company.getCompanySymbol());
			logger.debug("CompanyServiceImpl - allCompanies method - company :{}", companyRes.getCompanyName());
			allCompanies.add(companyRes);
		}
		logger.info("CompanyServiceImpl - allCompanies method - end");
		return allCompanies;
	}

}
