package com.naga.share.market.fundamental.analysis.root.services.industry;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.industry.IndustryDAO;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;

@Service
public class IndustryServiceImpl implements IndustryService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	IndustryDAO industryDAO;

	@Override
	public List<IndustryResponseDTO> allIndustries() {
		logger.info("IndustryServiceImpl - allIndustries method - start");
		List<IndustryResponseDTO> allIndustries = new ArrayList<IndustryResponseDTO>();
		for (Industry industry : industryDAO.findAllIndustries()) {
			IndustryResponseDTO industryRes = new IndustryResponseDTO();
			industryRes.setIndustryName(industry.getIndustryName());
			logger.debug("IndustryServiceImpl - allIndustries method - industry :{}", industryRes.getIndustryName());
			allIndustries.add(industryRes);
		}
		logger.info("IndustryServiceImpl - allIndustries method - end");
		return allIndustries;
	}

}
