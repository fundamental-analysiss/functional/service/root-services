package com.naga.share.market.fundamental.analysis.root.services.industry;

import java.util.List;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface IndustryService {

	/**
	 * 
	 * Service method to get all available industries 
	 * 
	 * @return List<IndustryResponse>
	 */
	public List<IndustryResponseDTO> allIndustries();
}
