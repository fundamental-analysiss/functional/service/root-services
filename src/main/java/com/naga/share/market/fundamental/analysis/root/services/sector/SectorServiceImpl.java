package com.naga.share.market.fundamental.analysis.root.services.sector;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.sector.SectorDAO;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;
import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;

@Service
public class SectorServiceImpl implements SectorService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	SectorDAO sectorDAO;

	@Override
	public List<SectorResponseDTO> allSectors() {
		logger.info("SectorServiceImpl - allSectors method - start");
		List<SectorResponseDTO> allSectors = new ArrayList<SectorResponseDTO>();
		for (Sector sector : sectorDAO.findAllSectors()) {
			SectorResponseDTO sectorRes = new SectorResponseDTO();
			sectorRes.setSectorName(sector.getSectorName());
			logger.debug("SectorServiceImpl - allSectors method - sector :{}", sectorRes.getSectorName());
			allSectors.add(sectorRes);
		}
		logger.info("SectorServiceImpl - allSectors method - end");
		return allSectors;
	}

}
