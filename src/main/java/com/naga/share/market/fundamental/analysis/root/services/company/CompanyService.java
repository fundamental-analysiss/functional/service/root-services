package com.naga.share.market.fundamental.analysis.root.services.company;

import java.util.List;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface CompanyService {

	/**
	 * Method to get all available companies
	 * 
	 * @return List<CompanyResponse>
	 */
	public List<CompanyResponseDTO> allCompanies();
}
